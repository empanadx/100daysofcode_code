# Code from #100DaysOfCode challenge

Some of the exercises, tutorials, and own scripts I've done as part of my [#100DaysOfCode](https://bitbucket.org/empanadx/a_100daysofcode/src/master/) Challenge

## Web Scripting
1. Automated web crawler using puppetteer + chromium [from tutorial](/AutomatedScripting/scrape.js) [own version](/AutomatedScripting/scrape_pinterest.js)
2. Web Workers (multi-threaded programming) [tutorial code with own notes](#)

## Javascript Basic Algorithm Scripting
diff algorithm problems, but fairly basic too. Includes exercise in code pen

1. Chunky Monkey ([JS]()) ([CodePen example](#))
2. Mutations ([JS](https://bitbucket.org/empanadx/freecodeschool/src/master/mutations.js)) ([CodePen example](#))
3. 