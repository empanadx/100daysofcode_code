// my version of scrape.js for a pinterest board!
const puppeteer = require('puppeteer');

let scrape = async () => {
  // Actual Scraping goes Here...
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();
  await page.goto('https://www.pinterest.com/jicamaychile/inspiraci%C3%B3n/');
  await page.waitFor(1000);
  
  // Scrape

  const result = await page.evaluate( () =? {
  let data = []; // Create an empty array
  let elements = document.querySelectorAll('.pinWrapper'); 
  // Select all 

  // Loop through each product
  for(var element in elements){
    // Select the title inside h1
    let imageTitle = element.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].attributes[0].value
    let imageSource = element.childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].childNodes[0].attributes[2].value;
    let pinURL = element.childNodes[0].childNodes[0].href;
    let pinDesc = element.childNodes[1].childNodes[0].childNodes[0].childNodes[0].childNodes[0].innerHTML;
  
    data.push({imageTitle, imageSource, pinURL, pinDesc}); // Push the data to array
  }
  
  return data; // Return data array 

  });

  // close browser and return data
  browser.close();
  return result;
};

scrape().then((value) => {
    // output scraped data to a file
});
