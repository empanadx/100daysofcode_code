// Workers allow you to run background threads so that your main program can take care of simple logic.
/* Workflow of a program with a worker:
    main.js:main thread      | worker.js: worker thread
    1.new worker created     |
    2.post a message ->      | 3.catch the message => {do something…}
    5. catch the result      | 4. <- post the result
*/
// worker side under webworkers_worker.js
let worker = new Worker('webworkers_worker.js');

let myObj = {name: 'Pablx', age: 43};
postMessage(myObj);