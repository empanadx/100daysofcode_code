var first = document.querySelector('#firstWord');
var second = document.querySelector('#secondWord');

var result = document.querySelector('.result');

// check if browser supports Worker api
if(window.Worker) {
    // requires script name as input
    var miWorker = new Worker('mdn_worker.js');
    
    // onchange registers only when the field is out of focus
    first.onchange = function(){
        miWorker.postMessage([first.value, second.value]);
        console.log('Message posted to worker');
    };
    
    second.onchange = function(){
        miWorker.postMessage([first.value, second.value]);
        console.log('Message posted to worker');
    };
    
    miWorker.onmessage = function(e){
        result.textContent = e.data;
        console.log('Message received from worker: ' + result.textContent);
    };
}
