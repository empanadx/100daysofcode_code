const puppeteer = require('puppeteer');

// function when called, returns a Promise, if a value is returned
// the Promise will resolve, or Reject if there is an error
async function getPic(){
    // await 'pauses' the function execution and wait for 'Promise' to resolve
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    // code will pause until page has loaded
    await page.goto('https://google.com');
    await page.setViewport({width: 1000, height: 500})
    // func takes object to customize location/name of file
    await page.screenshot({path: 'google.png'});

    await browser.close();
}

getPic();
