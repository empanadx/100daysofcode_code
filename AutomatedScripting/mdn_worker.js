// this is all you need (in this case)!!
onmessage = function(e){
    console.log('Message received from main script');
    var workerResult = '';
    var word1 = e.data[0];
    var word2 = e.data[1];
    
    if(word1 === word1.split("").reverse().join("")){
        workerResult = 'Your first word: ' + word1 + ' is a palindrome! <br/>') 
    }
    
    if(word2 === word2.split("").reverse().join("")){
        workerResult = 'Your second word: ' + word2 + ' is a palindrome!') 
    }
    
    console.log('Posting message back to main script'); 
    postMessage(workerResult);
}