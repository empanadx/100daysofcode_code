const puppeteer = require('puppeteer');

let scrape = async () => {
  // Actual Scraping goes Here...
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();
  await page.goto('http://books.toscrape.com/');
  await page.waitFor(1000);
  
  // Scrape
  
  //await page.click('li.col-xs-6:nth-child(1) > article:nth-child(1) > div:nth-child(1) > a:nth-child(1) > img:nth-child(1)');
  
  const result = await page.evaluate( () =? {
   let data = []; // Create an empty array
  let elements = document.querySelectorAll('.product_pod'); 
  // Select all 

  // Loop through each product
  elements.forEach(function(element){
    // Select the title inside h1
	var title = '';
	var price = '';
	if(typeof element.childNodes != 'undefined'){   
		title = element.childNodes[5].innerText;}
    // Select the price inside price_color class
    if(typeof element.childNodes != 'undefined'){
		price = element.childNodes[7].children[0].innerText;
	}
  
    data.push({title, price}); // Push the data to array
  });
  
  return data; // Return data array 
};

  });

  // close browser and return data
  browser.close();
  return result;
};

scrape().then((value) => {
    // logs value retrieved in scrape function defined above
    console.log(value); // Success!
});
